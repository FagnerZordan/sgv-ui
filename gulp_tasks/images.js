const gulp = require('gulp');
const conf = require('../conf/gulp.conf');

gulp.task('images', images);

function images() {
  return gulp.src(conf.path.src('assets/images/**/*'))
    .pipe(gulp.dest(conf.paths.tmp + '/assets/images/'));
}

