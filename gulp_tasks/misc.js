const path = require('path');

const gulp = require('gulp');
const del = require('del');
const filter = require('gulp-filter');
const rename = require('gulp-rename');

const conf = require('../conf/gulp.conf');

gulp.task('clean', clean);
gulp.task('other', other);

function clean() {
  return del([conf.paths.dist, conf.paths.tmp]);
}

function other() {
  const fileFilter = filter(file => file.stat.isFile());

  /*
   * The code below is necessary because Gulp does not
   * copy fonts from bower dependencies automatically.
   * Glyphicons is the most common example. It does
   * not copy fonts from other sources.
   */
  gulp.src(conf.path.bower('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest(`${conf.paths.dist}/fonts`));

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join(`!${conf.paths.src}`, '/**/*.{styl,js,html}')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(conf.paths.dist));
}
