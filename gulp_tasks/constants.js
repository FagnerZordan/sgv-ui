const gulp = require('gulp');
const gutil = require('gulp-util');
const myip = require('quick-local-ip');
const configConst = require('gulp-ng-config');
const replace = require('gulp-replace');

const conf = require('../conf/gulp.conf');
const env = process.env.ENV ? process.env.ENV : 'local';
const translation = process.env.TRANSLATION ? process.env.TRANSLATION : 'YES';

gulp.task('constants', constantsConfigs);

function constantsConfigs(done) {
  const forceip = gutil.env.forceip ? myip.getLocalIP4() : 'localhost';

  gulp.src('./config.json')
    .pipe(replace('localhost', forceip))
    .pipe(configConst(conf.ngModule, {
      createModule: false,
      environment: (env)
    }))
    .pipe(gulp.dest('./src/app/scripts'));

  gulp.src('./translation.json')
    .pipe(configConst(conf.ngModule, {
      createModule: false,
      environment: (translation)
    }))
    .pipe(gulp.dest('./src/app/scripts'));
  done();
}
