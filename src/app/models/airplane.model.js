/**
 *
 * This class represents aiplane
 *
 * @author Luiz Fagner <fagner.zordan@gmail.com.br>
 * @version 1.0.0 15/04/2018
 */
class Airplane {
  constructor(model) {
    this.id = model ? model.id : undefined;
    this.name = model ? model.name : undefined;
    this.capacity = model ? model.capacity : undefined;
    this.weight = model ? model.weight : undefined;
    this.supplier = model ? model.supplier : undefined;
  }

}
