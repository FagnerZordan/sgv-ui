/**
 *
 * This class represents flight
 *
 * @author Luiz Fagner <fagner.zordan@gmail.com.br>
 * @version 1.0.0 15/04/2018
 */
class Flight {
  constructor(model) {
    this.id = model ? model.id : undefined;
    this.departureTime = model ? model.departureTime : undefined;
    this.arrivalTime = model ? model.arrivalTime : undefined;
    this.origin = model && model.origin ? new City(model.origin) : undefined;
    this.destiny = model && model.destiny ? new City(model.destiny) : undefined;
    this.airplane = model && model.airplane ? new Airplane(model.airplane) : undefined;
    this.pilot = model && model.pilot ? new Pilot(model.pilot) : undefined;
    this.status = model ? model.flightStatus : undefined;
  }

}
