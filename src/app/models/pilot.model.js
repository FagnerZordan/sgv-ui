/**
 *
 * This class represents pilot
 *
 * @author Luiz Fagner <fagner.zordan@gmail.com.br>
 * @version 1.0.0 15/04/2018
 */
class Pilot {
  constructor(model) {
    this.id = model ? model.id : undefined;
    this.name = model ? model.name : undefined;
    this.lastName = model ? model.lastName : undefined;
    this.age = model ? model.age : undefined;
    this.flightCount = model ? model.flightCount : undefined;
  }

}
