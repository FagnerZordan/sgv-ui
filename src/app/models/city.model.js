/**
 *
 * This class represents the city
 *
 * @author Luiz Fagner <fagner.zordan@gmail.com.br>
 * @version 1.0.0 15/04/2018
 */
class City {
  constructor(model) {
    this.id = model ? model.id : undefined;
    this.state = model ? model.state : undefined;
    this.country = model ? model.country : undefined;
  }

}
