/**
 *
 * BlockUI global configuration.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function BlockUIConfigurer(blockUIConfig) {
    blockUIConfig.autoBlock = false;
    blockUIConfig.autoInjectBodyBlock = false;
    blockUIConfig.blockBrowserNavigation = true;
    blockUIConfig.delay = 100;
    blockUIConfig.template = '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>';
}

angular.module('app').config(BlockUIConfigurer);
BlockUIConfigurer.$inject = ['blockUIConfig'];