/**
 *
 * Angular $httpProvider configuration.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function HttpProviderConfigurer($httpProvider) {
  $httpProvider.defaults.withCredentials = false;
  $httpProvider.interceptors.push('AuthInterceptor');
}

angular.module('app').config(HttpProviderConfigurer);
HttpProviderConfigurer.$inject = ['$httpProvider'];
