/**
 *
 * $translateProvider configuration
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function TranslateProviderConfigurer($translateProvider, envConfig, translationConfig) {
  if (translationConfig.enabled) {
    $translateProvider.preferredLanguage('pt-BR');
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
  }
}

angular.module('app').config(TranslateProviderConfigurer);
TranslateProviderConfigurer.$inject = ['$translateProvider', 'envConfig', 'translationConfig'];
