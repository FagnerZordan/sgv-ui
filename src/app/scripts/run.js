/**
 *
 * Application initialization function
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function Initialize($rootScope, $state, AuthenticationService, $anchorScroll, $timeout, $translate, envConfig) {

  $rootScope.$state = $state;

  $anchorScroll.yOffset = 50;

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    $state.go('flight-list');
  });
}

angular.module('app').run(Initialize);
Initialize.$inject = ['$rootScope', '$state', 'AuthenticationService', '$anchorScroll', '$timeout', '$translate', 'envConfig'];
