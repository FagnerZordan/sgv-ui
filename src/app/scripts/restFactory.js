/**
 *
 * Factory for handling REST APIs.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function RestFactory(Restangular) {
  const RESTful = new function () {
    const that = this;

    this.one = function (arg) {
      return Restangular.one(arg);
    };

    this.resources = function (args) {
      let base;
      args.forEach((element) => {
        if (base) {
          base = base.all(element);
        } else {
          base = Restangular.all(element);
        }
      });
      return base;
    };

    this.resource = function (arg) {
      return that.resources([arg]);
    };

    this.resourcesFormData = function (args) {
      return that.resources(args).withHttpConfig({
        transformRequest: angular.identity
      });
    };

    this.resourceFormData = function (arg) {
      return that.resource(arg).withHttpConfig({
        transformRequest: angular.identity
      });
    };
  };

  return RESTful;
}

angular.module('app').factory("RestFactory", RestFactory);
RestFactory.$inject = ['Restangular'];
