/**
 *
 * Restangular global configuration.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function RestangularConfigurer(RestangularProvider, envConfig) {
  RestangularProvider.setBaseUrl(envConfig.rootURL);
  RestangularProvider.setPlainByDefault(true);
}
angular.module('app').config(RestangularConfigurer);
RestangularConfigurer.$inject = ['RestangularProvider', 'envConfig'];
