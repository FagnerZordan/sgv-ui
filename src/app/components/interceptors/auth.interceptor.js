/**
 *
 * AuthenticationInterceptor intercepts all http request
 * in order to handle with HTTP 401 / 403 response errors.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function AuthInterceptor($rootScope, $q) {
  return {
    responseError: rejection => {
      $rootScope.$broadcast({
        401: 'NOT_AUTHENTICATED',
        403: 'NOT_AUTHORIZED'
      }[rejection.status], rejection);
      return $q.reject(rejection);
    }
  };
}
angular.module('app').factory('AuthInterceptor', AuthInterceptor);
AuthInterceptor.$inject = ['$rootScope', '$q'];
