/**
 *
 * The AuthenticationService exposes the application login and logout features.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function AuthenticationService(RestFactory, $window, $state, $rootScope, Flash) {

  const CURRENT_USER = 'currentUser';
  const LOGIN = 'login';

  var previousPageBeforeLogin = undefined;

  const HEADERS = {
    'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
  };

  return {};

}
angular.module('app').factory('AuthenticationService', AuthenticationService);
AuthenticationService.$inject = ['RestFactory', '$window', '$state', '$rootScope', 'Flash'];
