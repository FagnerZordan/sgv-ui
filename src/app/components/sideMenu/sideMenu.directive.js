/**
 *
 * Side menu directive.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0 15/04/2018
 */
function sideMenu($state, $filter, AuthenticationService, $rootScope, $timeout) {
  var directive = {
    restrict: 'E',
    templateUrl: 'app/components/sideMenu/sideMenu.html',
    replace: true,
    link: directiveLink
  };

  return directive;

  /**
   * Unbind event listeners when $scope is destroyed.
   *
   * @param {Object} scope Angular $scope object
   * @param {Object} element A jQuery element
   * @param {String} eventType An string representing the event type (eg. 'click')
   */
  function unbindListenerOnDestroy($scope, element, eventType) {
    $scope.$on('$destroy', () => {
      element.off(eventType);
    });
  }

  /**
   * Binds a click listener, that executes once,
   * on html and body elements to close menu.
   *
   * @param {Object} element The menu element
   */
  function closeOnFocusOut(element) {
    angular.element('.header-menu-button.close-btn').one('click', () => {
      element.removeClass('active');
      closeSubMenus();
    });
  }

  /**
   * Hide all submenus (elements with '.sub-menu' class)
   */
  function closeSubMenus() {
    angular.element('.sub-menu').removeClass('active');
  }

  /**
   * Link function for DOM manipulation
   * */
  function directiveLink($scope, $element, $attrs) {

    $scope.hasRouteActive = (id) => {
      const elements = angular.element(id).children();
      for (let i = 0; i < elements.length; i++) {
        if($filter('includedByState')(elements[i].children[0].attributes["ui-sref"].nodeValue)) {
            return true;
        }
      }
    };

    /**
     * Display all elements with specific class (menuClass)
     */
    $scope.openMenu = menuClass => {
      angular.element(menuClass).addClass('active');
    };

    $element.on('click', e => {
      if(!$scope.mobile) {
        e.stopPropagation();
        $element.addClass('active');
        closeOnFocusOut($element);
      }
    });

    angular.element('.menu-link').on('click', () => {
      angular.element('body,html').off('click');
    });

    $scope.onSideMenuHover = () => {
      angular.element('select').blur();
    };

    $timeout(() => {
    /** For mobile devices only */
    if ($attrs.hasOwnProperty('mobileButtonId')) {
      $scope.mobile = true;
      const buttonElement = angular.element(`#${$attrs.mobileButtonId}`);
      buttonElement.on('click', e => {
        e.stopPropagation();
        $element.toggleClass('active');
        closeOnFocusOut($element);
      });
      unbindListenerOnDestroy($scope, buttonElement, 'click');
    }
    unbindListenerOnDestroy($scope, $element, 'click');
    });
  }
}

angular.module('app').directive('sideMenu', sideMenu);
sideMenu.$inject = ['$state', '$filter', 'AuthenticationService', '$rootScope', '$timeout'];
