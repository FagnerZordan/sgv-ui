/**
 *
 * Header Menu Directive
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0 15/04/2018
 */
function headerMenu() {
  const directive = {
    restrict: 'E',
    templateUrl: 'app/components/headerMenu/headerMenu.html',
    replace: true,
    controller: HeaderMenuController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

angular.module('app').directive('headerMenu', headerMenu);
HeaderMenuController.$inject = ['$state','$rootScope', 'AuthenticationService'];

function HeaderMenuController($state, $rootScope, AuthenticationService) {
  const vm = this;

  function init() {}

  init();
}
