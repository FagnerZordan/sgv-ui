/**
 *
 * Flight Controller
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com.br>
 * @version 1.0.0 15/04/2018
 */
function FlightFormController($state, RestFactory, FlightService) {
  const vm = this;

  /**
   * Get Flight By Id
   */
  vm.getFlight = (id) => {
    /* global Flight */
    RestFactory.resources(['flight']).get(id).then(response => {
      vm.flight = new Flight(response);
    });
  }

  /**
   * Init function to get calculation methods
   */
  function init() {
    vm.getFlight($state.params.id);
  }

  init();
}

angular.module('app').controller('FlightFormController', FlightFormController);
FlightFormController.$inject = ['$state', 'RestFactory', 'FlightService'];
