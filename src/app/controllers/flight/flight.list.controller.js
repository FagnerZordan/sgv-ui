/**
 *
 * Flight List Controller.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com.br>
 * @version 1.0 15/04/2018
 */
function FlightListController($controller, $state, $translate, FlightService, $timeout) {
  const vm = this;

  vm.sortBy = propertyName => {
    vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse : false;
    vm.propertyName = propertyName;
  };

  vm.getAllFlight = () => {
    FlightService.all().then(response => {
      vm.flights = response;
    });
  };

  vm.goToFlightForm = flightId => {
    $state.go('flight-form', {
      id: flightId
    });
  };

  function init() {
    vm.reverse = false;
    vm.getAllFlight();
  }

  init();
}

angular.module('app').controller('FlightListController', FlightListController);
FlightListController.$inject = ['$controller', '$state', '$translate', 'FlightService', '$timeout'];
