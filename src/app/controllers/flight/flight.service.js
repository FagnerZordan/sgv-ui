/**
 *
 * Service that provide methods to handle Flight objects.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com.br>
 * @version 1.0.0 15/04/2018
 */
angular.module('app').factory('FlightService', FlightService);
FlightService.$inject = ['RestFactory','$q'];

function FlightService(RestFactory, $q) {
  
  const buildNoPagingList = serverResponse => {
    const deferred = $q.defer();
    const flights = [];
    serverResponse.forEach(flightModel => {
      /* global Usuario*/
      flights.push(new Flight(JSON.parse(flightModel)));
    });
    deferred.resolve(flights);
    return deferred.promise;
  };

  const all = () => {
    return RestFactory.resource('flight').customGET()
    .then(response => {
      return buildNoPagingList(response);
    });
  };

  const userService = {
    all: all
  };

  return userService;
}
