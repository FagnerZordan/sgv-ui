/**
 *
 * Application routes configuration
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0.0 15/04/2018
 */
function StateConfigurer($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('');
  $urlRouterProvider.otherwise('flight/list');

  $stateProvider
    .state('flight-form', {
      url: '/flight/form/:id',
      views: {
        mainContent: {
          templateUrl: 'app/controllers/contentView.html'
        },
        sideMenu: {
          templateUrl: 'app/controllers/sideMenuView.html'
        },
        'content@flight-form': {
          templateUrl: 'app/controllers/flight/flightForm.html',
          controller: 'FlightFormController',
          controllerAs: 'vm'
        }
      },
      data: {
        allowAnonymous : true
      }
    })
    .state('flight-list', {
      url: '/flight/list',
      views: {
        mainContent: {
          templateUrl: 'app/controllers/contentView.html'
        },
        sideMenu: {
          templateUrl: 'app/controllers/sideMenuView.html'
        },
        'content@flight-list': {
          templateUrl: 'app/controllers/flight/flightList.html',
          controller: 'FlightListController',
          controllerAs: 'vm'
        }
      },
      data: {
        allowAnonymous : true
      }
    });
}

angular.module('app').config(StateConfigurer);
StateConfigurer.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
