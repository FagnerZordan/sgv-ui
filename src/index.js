angular.module('app', [
  'ngAnimate',
  'ngCookies',
  'ngFlash',
  'ngSanitize',
  'ngTouch',
  'pascalprecht.translate',
  'restangular',
  'ui.bootstrap',
  'ui.router',
  'blockUI',
  'isteven-multi-select',
  'ui.sortable',
  'ngMessages',
  'ngFileSaver',
  'gm.datepickerMultiSelect'
]);
