module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0,
    'prefer-const': 'warn',
    'no-unused-vars': 'warn',
    'no-trailing-spaces': 2,
    'generator-star-spacing': ['error', {'before': true, 'after': false}],
    'linebreak-style': ["error", "unix"],
    'quote-props': ["error", "consistent-as-needed"]
  }
}
