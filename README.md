
# SGV - SISTEMA GERENCIADOR DE VÔO - README
===========================================

Ambiente de Desenvolvimento
-------------

> **Pre-requisitos:**

> - NodeJS
> - Acesso ao repositório do sistema: https://bitbucket.org/FagnerZordan/sgv-ui

> **Instalando os pacotes:**
> - Executar o comando: npm install

> **Execução do sistema:**

> - Executar o comando: npm run serve
> - Acessar o endereço http:localhost:3000 para verificar que a aplicação está funcionando
