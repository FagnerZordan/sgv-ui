const conf = require('./gulp.conf');

module.exports = function () {
  return {
    ui: {
      port: 8081,
      weinre: {
        port: 9090
      }
    },
    server: {
      baseDir: [
        conf.paths.tmp,
        conf.paths.src
      ],
      routes: {
        '/bower_components': 'bower_components'
      },
    },
    open: false
  };
};
