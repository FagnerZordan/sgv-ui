const conf = require('./gulp.conf');
const wiredep = require('wiredep');

module.exports = function listFiles() {
  const wiredepOptions = Object.assign({}, conf.wiredep, {
    dependencies: true,
    devDependencies: true
  });

  const patterns = wiredep(wiredepOptions).js.concat([
    conf.path.tmp('index.js'),
    conf.path.tmp('routes.js'),
    conf.path.tmp('app/models/**/*.js'),
    conf.path.tmp('app/scripts/*.js'),
    conf.path.tmp('app/components/interceptors/*.js'),
    conf.path.tmp('app/components/filters/*.js'),
    conf.path.tmp('app/components/services/*.js'),
    conf.path.tmp('app/constants/*.js'),
    conf.path.tmp('app/controllers/**/*.js'),
    conf.path.src('**/*.html'),
    conf.path.tests('**/*spec.js'),
    /* List of directive that have test */
    conf.path.tmp('app/components/confirmationModal/*.js'),
    conf.path.tmp('app/components/personalDetails/*.js'),
    conf.path.tmp('app/components/searchFilterModal/*.js'),
    conf.path.tmp('app/components/paginationControl/*.js'),
    conf.path.tmp('app/components/selectGeneralTripData/*.js'),
    conf.path.tmp('app/components/addPassengerOnProgrammingModal/*.js'),
    conf.path.tmp('app/components/addStopsModal/*.js'),
    conf.path.tmp('app/components/listStopsModal/*.js')
  ]);

  const files = patterns.map(pattern => ({pattern}));
  files.push({
    pattern: conf.path.src('assets/**/*'),
    included: false,
    served: true,
    watched: false
  });

  files.push({
    pattern: conf.path.tests('**/api_mock/*.json'),
    watched: false,
    included: false,
    served: true
  });

  return files;
};
