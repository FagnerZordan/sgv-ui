const conf = require('./gulp.conf');
const listFiles = require('./karma-files.conf');

module.exports = function (config) {
  const configuration = {
    basePath: '../',
    colors: true,
    singleRun: true,
    autoWatch: false,
    logLevel: 'INFO',
    reporters: [
      'spec',
      'junit',
      'coverage'
    ],
    browsers: [
      'PhantomJS'
    ],
    frameworks: [
      'phantomjs-shim',
      'jasmine',
      'angular-filesort'
    ],
    files: listFiles(),
    preprocessors: {
      '**/src/app/**/*!(*spec).js': ['coverage'],
      [conf.path.src('**/*.html')]: [
        'ng-html2js'
      ]
    },
    ngHtml2JsPreprocessor: {
      stripPrefix: `${conf.paths.src}/`,
      moduleName: 'app'
    },
    angularFilesort: {
      whitelist: [
        conf.path.tmp('**/!(*.html|*.spec|*.mock).js')
      ]
    },
    junitReporter: {
      // outputDir: 'test-reports'
      outputFile: 'results/TESTS-xunit.xml',
      suite: 'sgv-ui',
      useBrowserName: false
    },
    coverageReporter: {
        // specify a common output directory
        dir: 'results',
        type: 'lcov',
        subdir: '.',
        instrumenterOptions: {
            istanbul: { noCompact: true }
        },
        includeAllSources: true
    },
    plugins: [
      require('karma-jasmine'),
      require('karma-junit-reporter'),
      require('karma-coverage'),
      require('karma-phantomjs-launcher'),
      require('karma-phantomjs-shim'),
      require('karma-ng-html2js-preprocessor'),
      require('karma-angular-filesort'),
      require('karma-spec-reporter')
    ]
  };

  config.set(configuration);
};
