/**
 * 
 * Flight List Controller Test.
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0 15/04/2018
 */
'use sctrict';

describe('Flight List Controller Test', function () {
  var control, state, restFctry, httpBackend, _FlightService;

  // Runs before each test
  beforeEach(function() {
    angular.mock.module('app');
  });

  // Runs before each test
  beforeEach(inject(function($controller, $httpBackend, RestFactory, $state, $stateParams, FlightService) {
    httpBackend = $httpBackend;
    restFctry = RestFactory;
    state = $state;
    _FlightService = FlightService;

    control = $controller('FlightListController');

    // mocks
    initMocks();
  }));

  // Creates mocks
  function initMocks() {
    state.current.name = 'flight-list';

    // setup the data we wish to return for the success function in the controller
    // we use the $httpBackend to create a mock response
    httpBackend.whenGET(/vehicle-types.*/).respond(200, {
      "content": [1, 2, 3, 4]
    });

    // use a Jasmine Spy
    spyOn(state, 'go');
    spyOn(restFctry, 'resource').and.callThrough();
    spyOn(_FlightService, 'all').and.callThrough();
  }

  it('Should call the flight-form route', function () {
    const flightId = 1;
    control.goToFlightForm(flightId);
    expect(state.go).toHaveBeenCalledWith('flight-form', jasmine.any(Object));
  });

});
