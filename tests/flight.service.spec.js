/**
 *
 * Flight Service Test
 *
 * @author Luiz Fagner Zordan <fagner.zordan@gmail.com>
 * @version 1.0 15/04/2018
 */
'use sctrict';

describe('Flight Service Test', function () {

  var _FlightService,
      _RestFactory,
      flight,
      deferred,
      _scope;

  // Runs before each test
  beforeEach(function () {
    angular.mock.module('app');
  });

  beforeEach(inject(function (FlightService, RestFactory, $q, $rootScope) {
    _FlightService = FlightService;
    _RestFactory   = RestFactory;
    _scope         = $rootScope.$new();

    deferred = $q.defer();

    spyOn(_RestFactory, 'resource').and.returnValue({
      customGET: jasmine.createSpy('customGET').and.returnValue(deferred.promise),
    });

  }));

  it('Should service to be defined', function () {
    expect(_FlightService).toBeDefined();
  });

  it('Should list all flights', function () {
    _FlightService.all();
    expect(_RestFactory.resource).toHaveBeenCalledWith('flight');
  });

});
